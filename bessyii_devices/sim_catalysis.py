"""
Module for Simulating Devices in a Catalysis Beamline

This module contains classes to simulate various devices used in a catalysis beamline setup.
The simulated devices include monochromators, apertures, mirrors, N-dimensional detectors,
reactor cell controllers, gas dosimeters, and sample transfer systems. Each class uses
`ophyd.Device` and `ophyd.Signal` components to emulate the behavior and attributes of
real-world experimental devices.

Classes:
--------
- SimMonochromator: Represents a simulated monochromator using one SynAxis.
- SimAperture: Represents a simulated aperture using four SynAxis.
- SimMirror: Represents a simulated mirror using six SynAxis.
- SimNDDetector: Represents a simulated N-dimensional detector device with configurable parameters.
- SimReactorCellController: Represents a simulated reactor cell controller with temperature control.
- SimGasDosimeter: Represents a simulated gas dosimeter device with multiple mass flow controllers.
- SimSampleSelector: Represents a simulated sample selector device with three axes (x, y, z).
- SimSampleTransfer: Represents a simulated sample transfer device consisting of a magazine and a selector.

Usage:
------
Import the required classes from this module and instantiate them as needed for simulation purposes.
If the device instantiation is organized using the happi library, change the device configuration file y(a)ml
according to the requirements of the simulation.

Notes:
------
- This module uses the `ophyd` library to create simulated devices.
- The classes are designed to mimic the behavior of real beamline components, allowing for testing
  and development without requiring access to physical hardware.

"""

import builtins
import inspect
import threading
import time
from typing import Any, Dict, List, Tuple, Union

import numpy as np
from ophyd import Component as Cpt
from ophyd import Device, Kind, Signal
from ophyd.sim import SynAxis
from ophyd.status import DeviceStatus


class SimMonochromator(Device):
    """
    A class that represents a simulated monochromator using 1 SynAxis.
    """

    energy: SynAxis = Cpt(
        cls=SynAxis, name="energy", kind=Kind.normal, labels=("monochromators",)
    )


class SimAperture(Device):
    """
    A class that represents a simulated aperture using 4 SynAxis.
    """

    top: SynAxis = Cpt(cls=SynAxis, name="top", kind=Kind.normal, labels=("apertures",))
    bottom: SynAxis = Cpt(
        cls=SynAxis, name="bottom", kind=Kind.normal, labels=("apertures",)
    )
    left: SynAxis = Cpt(
        cls=SynAxis, name="left", kind=Kind.normal, labels=("apertures",)
    )
    right: SynAxis = Cpt(
        cls=SynAxis, name="right", kind=Kind.normal, labels=("apertures",)
    )


class SimMirror(Device):
    """
    A class that represents a simulated mirror using 6 SynAxis.
    """

    tx: SynAxis = Cpt(cls=SynAxis, name="tx", kind=Kind.normal, labels=("mirrors",))
    ty: SynAxis = Cpt(cls=SynAxis, name="ty", kind=Kind.normal, labels=("mirrors",))
    tz: SynAxis = Cpt(cls=SynAxis, name="tz", kind=Kind.normal, labels=("mirrors",))
    rx: SynAxis = Cpt(cls=SynAxis, name="rx", kind=Kind.normal, labels=("mirrors",))
    ry: SynAxis = Cpt(cls=SynAxis, name="ry", kind=Kind.normal, labels=("mirrors",))
    rz: SynAxis = Cpt(cls=SynAxis, name="rz", kind=Kind.normal, labels=("mirrors",))


class SimNDDetector(Device):
    """
    A class that represents a simulated N-dimensional detector device.

    This class simulates an N-dimensional detector device for data acquisition in experimental setups.
    It allows users to configure various parameters such as gain, exposure time, resolution, and more.
    The detector can be triggered to initiate data acquisition, and the acquired data.
    The acquired data are random floats uniformly distributed over [min_counts, max_counts) and can be accessed through the `counts` attribute.
    The type (int or float) of acquired data is configurable.

    Attributes:
    -----------
    counts : Signal
        A signal that stores the counts obtained from the detector.
    counts_shape : tuple | list
        The shape of the count array representing the dimensions of the detector.
    counts_dtype : type (written as a string), optional
        The data type of counts stored in the detector. Possible values: "int", "float". Default is "int".
    min_counts : Signal
        The minimum possible count value (lower bound incl. for random.uniform).
    max_counts : Signal
        The maximum possible count value (upper bound excl. for random.uniform).
    gain : Signal
        A signal that allows setting the gain on a channel of the detector.
    exposure_time : Signal
        A signal that allows setting the exposure time on a channel of the detector.
    clear : Signal
        A signal that allows preparing the detector for data acquisition.
    resolution : Signal
        The spatial resolution of the detector.
    dynamic_range : Signal
        The dynamic range of the detector.
    dead_time : Signal
        The dead time of the detector.
    trigger_mode : Signal
        The trigger mode of the detector.
    cooling_system : Signal
        The cooling system status of the detector.
    calibration_params : Signal
        Calibration parameters of the detector.
    verbose_on : bool, optional
        Controls whether verbose output is enabled. Default is False.

    Methods:
    --------
    __init__(*args, **kwargs)
        Initialize the simulated N-dimensional detector with configurable parameters.
    stage()
        Called when the device is being prepared for data acquisition, i.e. preliminary setup before data collection
    unstage()
        Called when the device has finished data acquisition. Resets the device to a clean state becaus of changes made during staging.
    trigger()
        Simulates triggering the detector to initiate data acquisition.
    read()
        Simulates reading of the current state of the detector.

    Parameters:
    -----------
    counts_shape : tuple or list, optional
        The shape of the count array representing the dimensions of the detector. Default is (1024, 1024, 1024).
    counts_dtype : type (written as a string), optional
        The data type of counts stored in the detector. Possible values: "int", "float". Default is "int".
    min_counts : float, optional
        The minimum count value that can be detected by the detector. Default is 1.0.
    max_counts : float, optional
        The maximum count value that can be detected by the detector. Default is 1000.0.
    gain : int, optional
        The gain setting for the detector. Default is 1.
    exposure_time : float, optional
        The exposure time for data acquisition. Default is 1 second.
    resolution : float, optional
        The spatial resolution of the detector. Default is 0.1.
    dynamic_range : int, optional
        The dynamic range of the detector. Default is 10000.
    dead_time : float, optional
        The dead time of the detector. Default is 0.1 seconds.
    trigger_mode : str, optional
        The trigger mode of the detector. Default is "continuous".
    cooling_system : str, optional
        The status of the cooling system of the detector. Default is "off".
    calibration_params : dict, optional
        Calibration parameters of the detector. Default is an empty dictionary.
    verbose_on : bool, optional
        Controls whether verbose output is enabled. Default is False.
    **kwargs : dict
        Additional keyword arguments to pass to the superclass.

    Raises
    ------
    ValueError
        If any of the provided parameters are invalid.
    """

    # A variable that stores counts_shape
    counts_shape: Union[Tuple[int, ...], List[int]] = tuple()
    # A variable that stores counts_dtype
    counts_dtype: str = "int"
    # A variable that stores verbose_on
    verbose_on: bool = False

    # Signal declarations:

    # A signal that stores counts from the detector.
    counts: Signal = Cpt(Signal, kind=Kind.hinted, labels=("detectors",))
    # A signal that enables us to set the lowest possible count value.
    min_counts: Signal = Cpt(Signal, kind=Kind.config, labels=("detectors",))
    # A signal that enables us to set the highest possible count value.
    max_counts: Signal = Cpt(Signal, kind=Kind.config, labels=("detectors",))
    # A signal that let's us set the gain on a channel of the detector.
    gain: Signal = Cpt(Signal, kind=Kind.config, labels=("detectors",))
    # A signal that let's us set the exposure time on a channel of detector.
    exposure_time: Signal = Cpt(Signal, kind=Kind.config, labels=("detectors",))
    # A signal which allows us to prepare the detector for data acquisition. Default value: 1
    clear: Signal = Cpt(Signal, kind=Kind.omitted, value=1, labels=("detectors",))
    # A signal that enables us to set the spatial resolution of the detector.
    resolution: Signal = Cpt(Signal, kind=Kind.config, labels=("detectors",))
    # A signal that enables us to set the dynamic range of the detector.
    dynamic_range: Signal = Cpt(Signal, kind=Kind.config, labels=("detectors",))
    # A signal that enables us to set the dead time of the detector. During the dead time, the detector is typically recovering from the previous event and is unable to process or detect subsequent events.
    dead_time: Signal = Cpt(Signal, kind=Kind.config, labels=("detectors",))
    # A signal that enables us to set the trigger mode of the detector.
    trigger_mode: Signal = Cpt(Signal, kind=Kind.config, labels=("detectors",))
    # A signal that enables us to set the cooling system status of the detector.
    cooling_system: Signal = Cpt(Signal, kind=Kind.config, labels=("detectors",))
    # A signal that enables us to set the calibration parameters of the detector.
    calibration_params: Signal = Cpt(Signal, kind=Kind.config, labels=("detectors",))

    def __init__(
        self,
        *args,
        counts_shape: Union[Tuple[int, ...], List[int]] = (1024,) * 3,
        counts_dtype: str = "int",
        min_counts: float = 1.0,
        max_counts: float = 1000.0,
        gain: int = 1,
        exposure_time: float = 1,
        resolution: float = 0.1,
        dynamic_range: int = 10000,
        dead_time: float = 0.1,
        trigger_mode: str = "continuous",
        cooling_system: str = "off",
        calibration_params: Dict[str, Any] = {},
        verbose_on: bool = False,
        **kwargs: Any,
    ) -> None:
        """
        Initialize the simulated N-dimensional detector with configurable parameters.

        Parameters:
        -----------
        *args : tuple
            Positional arguments to pass to the superclass.
        counts_shape : tuple or list, optional
            The shape of the count array representing the dimensions of the detector. Default is (1024, 1024, 1024).
        counts_dtype : type (written as a string), optional
            The data type of counts stored in the detector. Possible values: "int", "float". Default is "int".
        min_counts : float, optional
            The minimum count value that can be detected by the detector. Default is 1.0.
        max_counts : float, optional
            The maximum count value that can be detected by the detector. Default is 1000.0.
        gain : int, optional
            The gain setting for the detector. Default is 1.
        exposure_time : float, optional
            The exposure time for data acquisition. Default is 1 second.
        resolution : float, optional
            The spatial resolution of the detector. Default is 0.1.
        dynamic_range : int, optional
            The dynamic range of the detector. Default is 10000.
        dead_time : float, optional
            The dead time of the detector. Default is 0.1 seconds.
        trigger_mode : str, optional
            The trigger mode of the detector. Default is "continuous".
        cooling_system : str, optional
            The status of the cooling system of the detector. Default is "off".
        calibration_params : dict, optional
            Calibration parameters of the detector. Default is an empty dictionary.
        verbose_on : bool, optional
            Controls whether verbose output is enabled. Default is False.
        **kwargs : dict
            Additional keyword arguments to pass to the superclass.

        Raises
        ------
        ValueError
            If any of the provided parameters are invalid.
        """

        super().__init__(*args, **kwargs)

        # Set parameters
        self.counts_shape = counts_shape
        self.counts_dtype = counts_dtype
        self.verbose_on = verbose_on
        self.min_counts.set(min_counts)
        self.max_counts.set(max_counts)
        self.gain.set(gain)
        self.exposure_time.set(exposure_time)
        self.resolution.set(resolution)
        self.dynamic_range.set(dynamic_range)
        self.dead_time.set(dead_time)
        self.trigger_mode.set(trigger_mode)
        self.cooling_system.set(cooling_system)
        self.calibration_params.set(calibration_params)

        # Validate input parameters
        self._validate_parameters()
        # Print message if verbose_on=True
        self._print_message(
            f"{self.__class__.__name__}: {inspect.currentframe().f_code.co_name} finished."
        )

    def _print_message(self, message: str) -> None:
        """
        Print a message if verbose output is enabled.

        Parameters:
        -----------
        message : str
            The message to be printed.
        """
        if self.verbose_on:
            print(message)

    def _validate_parameters(self) -> None:
        """
        Validate the parameters set during initialization.

        This method ensures that all parameters set during initialization are valid.
        It checks whether counts dimensions are positive integers, gain and exposure time are positive,
        minimum counts are less than maximum counts, resolution, dynamic range, and dead time are positive,
        trigger mode is set to "continuous", and cooling system status is either "on" or "off".

        Raises
        ------
        ValueError
            If any of the provided parameters are invalid.
        """

        counts_shape: Union[Tuple[int, ...], List[int]] = self.counts_shape
        if any(d <= 0 for d in counts_shape):
            raise ValueError("Count array dimensions must be positive integers.")
        if self.counts_dtype not in ["int", "float"]:
            raise ValueError(
                f'counts_dtype must be either "int" or "float" written as a string. Current value: {self.counts_dtype}. Type of current value: {type(self.counts_dtype)}'
            )
        if self.verbose_on not in [True, False]:
            raise ValueError("verbose_on must be either True or False.")
        if self.gain.get() <= 0:
            raise ValueError("Gain must be positive.")
        if self.exposure_time.get() <= 0:
            raise ValueError("Exposure time must be positive.")
        if self.min_counts.get() > self.max_counts.get():
            raise ValueError(
                "Minimum counts must be less than or equal to maximum counts."
            )
        if self.resolution.get() <= 0:
            raise ValueError(f"Resolution must be positive. {self.resolution.get()}")
        if self.dynamic_range.get() <= 0:
            raise ValueError("Dynamic range must be positive.")
        if self.dead_time.get() < 0:
            raise ValueError("Dead time must be non-negative.")
        if self.trigger_mode.get() != "continuous":
            raise ValueError("Wrong trigger mode. Use: continuous.")
        if self.cooling_system.get() not in ["on", "off"]:
            raise ValueError("Cooling system wrong value. Use [on, off].")

        # Print message if verbose_on=True
        self._print_message(
            f"{self.__class__.__name__}: {inspect.currentframe().f_code.co_name} finished."
        )

    def stage(self) -> None:
        """
        Prepare the detector for data acquisition.

        Set the clear signal to 0.
        """
        self.clear.set(0)

        # Print message if verbose_on=True
        self._print_message(
            f"{self.__class__.__name__}: {inspect.currentframe().f_code.co_name} finished."
        )

    def unstage(self) -> None:
        """
        Simulate the cleanup of changes made during staging.

        This method simulates resetting the device to obtain a clean state before the next staging.
        """
        # Set the clear signal to 1.
        self.clear.set(1)

        # Reset counts to an empty array
        self.counts.set([])

        # Print message if verbose_on=True
        self._print_message(
            f"{self.__class__.__name__}: {inspect.currentframe().f_code.co_name} finished."
        )

    def _on_trigger_finished(self, status: DeviceStatus):
        """
        Callback function for trigger completion.
        """

        # Print message if verbose_on=True
        self._print_message(
            f"{self.__class__.__name__}: trigger finished. Device status: success={status.success} and done={status.done}."
        )

    def trigger(self) -> DeviceStatus:
        """
        Simulate the triggering of the detector to start data acquisition.

        This function simulates the waiting time caused by the exposure time and then simulates data acquisition
        by generating uniformly distributed random counts (int or floats) within the specified range.

        Returns
        -------
        status : DeviceStatus
            A DeviceStatus object indicating that data acquisition is finished.
            This object is used to track the status of the device and signals the
            completion of data acquisition when the method is called.
        """
        # Create a DeviceStatus object
        status = DeviceStatus(self)
        # Add callback
        status.add_callback(self._on_trigger_finished)

        def acquisition():
            try:
                # Check min_counts and max_counts
                min_counts = self.min_counts.get()
                max_counts = self.max_counts.get()

                if min_counts >= max_counts:
                    raise ValueError(
                        "Minimum counts must be less than or equal to maximum counts."
                    )

                # Check entries of counts_shape
                counts_shape = self.counts_shape
                if any(d <= 0 for d in counts_shape):
                    raise ValueError(
                        "Count array dimensions must be positive integers."
                    )

                # Check entry of counts_dtype
                if self.counts_dtype not in ["int", "float"]:
                    raise ValueError(
                        f'counts_dtype must be either "int" or "float" written as a string. Current value: {self.counts_dtype}. Type of current value: {type(self.counts_dtype)}'
                    )

                # Simulate the waiting time caused by the exposure time.
                exposure_time = self.exposure_time.get()
                time.sleep(exposure_time)

                # Simulate data acquisition.
                counts_dtype = getattr(builtins, self.counts_dtype)
                counts = np.random.uniform(
                    low=min_counts, high=max_counts, size=counts_shape
                ).astype(counts_dtype)
                self.counts.put(counts)

                # Mark the status as finished successfully.
                status.set_finished()

            except Exception as e:
                # Mark as finished but failed with the given Exception.
                status.set_exception(e)

        # Run the acquisition in a separate thread to avoid blocking caused by exposure time
        threading.Thread(target=acquisition).start()
        return status

    def read(self):
        """
        Simulate read of the current state of the detector.

        This method reads the current state of the detector and returns the result.
        It also prints a message indicating that the reading process has finished.

        Returns
        -------
        result : dict
            A dictionary containing the current state of the detector.

        """
        res = super().read()

        # Print message if verbose_on=True
        self._print_message(
            f"{self.__class__.__name__}: {inspect.currentframe().f_code.co_name} finished."
        )

        return res


class SimReactorCellController(Device):
    """
    A class that represents a simulated reactor cell controller.

    This class inherits from `ophyd.Device` and defines three signals to control and monitor
    the temperature of a simulated reactor cell. Each signal is an instance of `ophyd.Signal`.

    Attributes:
    -----------
    temperature_target : ophyd.Signal
        The target temperature for the reactor cell. This is a configuration parameter.

    temperature_ramp : ophyd.Signal
        The rate at which the temperature should be ramped up or down to the target temperature.
        This is a configuration parameter.

    temperature_sample : ophyd.Signal
        The current temperature of the reactor cell. This could be a "signalR0" but for simulation purposes this is a "signal"
    """

    temperature_target: Signal = Cpt(
        cls=Signal, name="temperature_target", kind=Kind.config, labels=("controllers",)
    )
    temperature_ramp: Signal = Cpt(
        cls=Signal, name="temperature_ramp", kind=Kind.config, labels=("controllers",)
    )
    temperature_sample: Signal = Cpt(
        cls=Signal, name="temperature_sample", kind=Kind.normal, labels=("controllers",)
    )


class SimGasDosimeter(Device):
    """
    A class that represents a simulated gas dosimeter device with multiple mass flow controllers (MFCs).

    This class defines three simulated mass flow controllers as components of the dosimeter. Each MFC is
    represented by an ophyd Signal, which can be used to monitor and control gas flow rates.

    Attributes
    ----------
    mfc_1 : Signal
        The signal representing the first mass flow controller (MFC).
    mfc_2 : Signal
        The signal representing the second mass flow controller (MFC).
    mfc_3 : Signal
        The signal representing the third mass flow controller (MFC).
    """

    mfc_1: Signal = Cpt(cls=Signal, name="mfc_1", labels=("dosimeters",))
    mfc_2: Signal = Cpt(cls=Signal, name="mfc_2", labels=("dosimeters",))
    mfc_3: Signal = Cpt(cls=Signal, name="mfc_3", labels=("dosimeters",))


class SimSampleSelector(Device):
    """
    A class that represents a simulated sample selector device with three axes (x, y, z).

    This class represents a simulated sample selector device with three
    orthogonal axes: x, y, and z. Each axis is an instance of the SynAxis
    class from ophyd.sim, which simulates the behavior of a motor axis.

    Attributes
    ----------
    x : SynAxis
        The simulated axis for the x-coordinate.
    y : SynAxis
        The simulated axis for the y-coordinate.
    z : SynAxis
        The simulated axis for the z-coordinate.
    """

    x: SynAxis = Cpt(cls=SynAxis, name="x", labels=("samples"))
    y: SynAxis = Cpt(cls=SynAxis, name="y", labels=("samples"))
    z: SynAxis = Cpt(cls=SynAxis, name="z", labels=("samples"))


class SimSampleTransfer(Device):
    """
    A class that represents a simulated sample transfer device.

    This device consists of:
        - magazin: A simulated axis representing the magazin.
        - selector: A simulated sample selector.
    """

    magazin: SynAxis = Cpt(SynAxis, name="magazin", labels=("samples"))
    selector: SimSampleSelector = Cpt(
        SimSampleSelector, name="selector", labels=("samples")
    )
